﻿using System.Threading.Tasks;

using NearClient.Utilities;

namespace NearClient
{
    public abstract class AccountCreator
    {
        public abstract Task CreateAccountAsync(string newAccountId, PublicKey publicKey);
    }
}