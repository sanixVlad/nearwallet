﻿namespace NearClient
{
    public class ProviderConfig
    {
        public dynamic Args { get; set; }
        public ProviderType Type { get; set; }
    }
}